# coding: utf-8

import glob
import mojimoji


read_path_list = sorted(glob.glob("/now20/f-hiroto/KNP/data/raw_24_divided/*"))
write_path = "/now20/f-hiroto/KNP/data/zen_24_divided/"

k = 0
for read_path in read_path_list:
	k += 1
	read_f = open(read_path, "r")
	write_f = open(write_path+"{}".format(k), "w")
	for line in read_f:
		try:
			write_f.write(mojimoji.han_to_zen(line))
		except StopIteration:
			break
	read_f.close()
	write_f.close()
